#ifndef COMMAND_HPP
# define COMMAND_HPP

# include<string>
# include<vector>

namespace KlassDB
{
	using namespace std;

	class Database;

	class Command
	{
		public:
			inline Command(const string&& name, const string&& usage,
				const string&& description)
				: name{name}, usage{usage}, description{description}
			{}

			inline const string& get_name() const
			{
				return name;
			}

			inline const string& get_usage() const
			{
				return usage;
			}

			inline const string& get_description() const
			{
				return description;
			}

			virtual void perform(Database& db, const vector<string>& args) const = 0;

		private:
			const string name;
			const string usage;
			const string description;
	};

	class HelpCommand : public Command
	{
		public:
			inline HelpCommand()
				: Command("help", "help", "Shows some help")
			{}

			void perform(Database& db, const vector<string>&) const override;
	};

	class ExitCommand : public Command
	{
		public:
			inline ExitCommand()
				: Command("exit", "exit", "Shuts the database down")
			{}

			void perform(Database&, const vector<string>&) const override;
	};

	class UptimeCommand : public Command
	{
		public:
			inline UptimeCommand()
				: Command("uptime", "uptime", "Prints database's uptime")
			{}

			void perform(Database& db, const vector<string>&) const override;
	};
}

#endif
