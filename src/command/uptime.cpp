#include "command.hpp"
#include "../klass_db.hpp"

using namespace KlassDB;

void UptimeCommand::perform(Database& db, const vector<string>&) const
{
	cout << db.get_uptime() << '\n';
}
