#include "klass_db.hpp"

using namespace KlassDB;

Database::Database()
{
	init_logs();

	register_command<HelpCommand>();
	register_command<ExitCommand>();
	register_command<UptimeCommand>();
}

unsigned long next_time_step(unsigned long& time, unsigned long size)
{
	const unsigned long t = time / size;
	time %= size;

	return t;
}

string Database::get_uptime() const
{
	auto time = (get_timestamp() - start) / 1000;
	string str = to_string(next_time_step(time, 31536000)) + " years, ";
	str += to_string(next_time_step(time, 2678400)) + " months, ";
	str += to_string(next_time_step(time, 86400)) + " days, ";
	str += to_string(next_time_step(time, 3600)) + " hours, ";
	str += to_string(next_time_step(time, 60)) + " minutes, ";
	str += to_string(time) + " seconds";

	return str;
}

void Database::init_logs()
{
	logs = ofstream(LOGS_FILE, ios::app);
}

void Database::log(const string&& log)
{
	const auto time = get_time();

	cout << '[' << time << "] " << log << '\n';
	logs << '[' << time << "] " << log << '\n';
	logs.flush();
}

void Database::init()
{
	data_manager = DataManager(*this);
	data_manager.init();

	processes_manager = ProcessesManager(*this);
	processes_manager.init();

	users_manager = UsersManager(*this);
	users_manager.init();

	clients_manager = ClientsManager(*this);
	clients_manager.init();
}

const Command* Database::get_command(const string& name) const
{
	for(const auto& c : commands) {
		if(c->get_name() == name) {
			return c.get();
		}
	}

	return nullptr;
}

bool Database::exec_command(const string& command)
{
	stringstream ss(command);
	string buffer;

	if(!getline(ss, buffer, ' ')) return false;

	const auto c = get_command(buffer);
	if(!c) return false;

	vector<string> args;

	while(getline(ss, buffer, ' ')) {
		args.push_back(buffer);
	}

	c->perform(*this, args);

	return true;
}
