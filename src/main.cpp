#include "klass_db.hpp"

using namespace std;

int main()
{
	using namespace KlassDB;

	Database database;
	database.log(string("Starting KlassDB version ") + VERSION);
	database.log("Initializing database...");

	database.init();

	database.log("Ready!");

	string command;

	while(true) {
		getline(cin, command);
		if(!database.exec_command(command)) {
			cout << "Unknown command. Type \"help\" to show some help." << '\n';
		}
	}

	return 0;
}
