#ifndef CLASS_PARSER_HPP
# define CLASS_PARSER_HPP

# include<memory>
# include<string>
# include<vector>

# include "../../util/util.hpp"

namespace KlassDB
{
	using namespace std;

	enum Type : uint8_t
	{
		VOID = 0,

		INT8 = 1,
		UINT8 = 2,
		INT16 = 3,
		UINT16 = 4,
		INT32 = 5,
		UINT32 = 6,
		INT64 = 7,
		UINT64 = 8,

		FLOAT = 9,
		DOUBLE = 10,

		STRING = 11,
		OBJECT = 12
	};

	struct Variable
	{
		const Type type;
		const string name;
		const void* value;
	};

	struct Function
	{
		const Type type;
		const string name;
		const vector<Variable> args;
	};

	class Class
	{
		public:
			Class() = default;
			inline Class(const string& name)
				: name{name}
			{}

			inline const string& get_name() const
			{
				return name;
			}

			inline vector<Variable>& get_variables()
			{
				return variables;
			}

			inline vector<Function>& get_functions()
			{
				return functions;
			}

		private:
			const string name;
			vector<Variable> variables;
			vector<Function> functions;
	};

	class parse_exception : public exception
	{
		public:
			inline parse_exception(const string& reason)
				: reason{reason}
			{}

			inline const char* what() const noexcept
			{
				return reason.c_str();
			}

		private:
			const string reason;
	};

	enum Punctuation : char
	{
		ASSIGN = '=',
		BRACKET_OPEN = '(',
		BRACKET_CLOSE = ')',
		COMMA = ','
	};

	struct Statement
	{
		const Type type;
		const string identifier;
		const vector<Statement> args;
		const vector<Statement> scope;
	};

	Statement parse_class(char** str);

	enum Operator : char
	{
		ADD = '+',
		SUBTRACT = '-',
		MULTIPLY = '*',
		DIVIDE = '/',
		MODULO = '%'
	};

	double eval_expr(const string& str);
}

#endif
