#ifndef UTIL_HPP
# define UTIL_HPP

# include<chrono>
# include<fstream>
# include<iostream>
# include<string>

namespace KlassDB
{
	using namespace std;

	unsigned long get_timestamp();
	string get_time();

	void trim(string& str);

	string read_file(const string& file);
}

#endif
