#include "util.hpp"

using namespace KlassDB;

string KlassDB::read_file(const string& file)
{
	ifstream stream(file);
	if(!stream.is_open()) return "";

	stream.seekg(0);
	stream.seekg(ios::end);

	const auto size = stream.tellg();
	string buffer(size, '\0');
	stream.read(&buffer[0], size);
	return buffer;
}
