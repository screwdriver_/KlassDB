NAME = KlassDB
COMPILER = gcc
FLAGS = -Wall -Wextra -Werror -std=c++17 -o3

SRC = src/*.cpp src/command/*.cpp src/manager/*.cpp src/parser/class/*.cpp src/util/*.cpp

LIBS = -lstdc++

all: $(NAME)

$(NAME):
	@$(COMPILER) $(FLAGS) -o $(NAME) $(SRC) $(LIBS)

clean:

fclean: clean
	@rm -rf $(NAME)

re: fclean all
